import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vs_flutter_catalog/models/catalog.dart';
import 'package:vs_flutter_catalog/widgets/drawer.dart';
import 'package:vs_flutter_catalog/widgets/item_widget.dart';
import 'dart:convert';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final int days = 30;
  final String name = 'Codepur';

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() async {
    final catalogJson =
        await rootBundle.loadString('assets/files/catalog.json');
    final decodedData = jsonDecode(catalogJson);
    var productsData = decodedData['products'];
    CatalogModel.itemList = List.from(productsData)
        .map<Item>((item) => Item.fromMap(item))
        .toList();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Catalog App'),
      ),
      body: (CatalogModel.itemList.isNotEmpty)
          ? ListView.builder(
              itemCount: CatalogModel.itemList.length,
              itemBuilder: (context, index) => ItemWidget(
                item: CatalogModel.itemList[index],
              ),
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
      drawer: const MyDrawer(),
    );
  }
}
