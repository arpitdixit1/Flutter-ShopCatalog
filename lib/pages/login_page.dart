import 'package:flutter/material.dart';
import 'package:vs_flutter_catalog/utils/constants.dart';
import 'package:vs_flutter_catalog/utils/routes.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String name = '';
  bool changeButton = false;

  final _formkey = GlobalKey<FormState>();

  // Move to HomePage
  moveToHome(BuildContext context) async {
    if (_formkey.currentState != null) {
      if (_formkey.currentState!.validate()) {
        setState(() {
          changeButton = true;
        });
        await Future.delayed(const Duration(seconds: 2));
        await Navigator.pushNamed(
          context,
          MyRoutes.homeRoute,
        );
        setState(() {
          changeButton = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Image.asset(
                'assets/images/hey_image.png',
              ),
              Text(
                'Welcome $name',
                style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 40.0),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Enter username',
                          labelText: 'Username',
                        ),
                        validator: (value) {
                          var confirmValue = value ?? '';
                          if (confirmValue.isEmpty) {
                            return 'Username cannot be empty';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            name = value;
                          });
                        },
                      ),
                      TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                          hintText: 'Enter password',
                          labelText: 'Password',
                        ),
                        validator: (value) {
                          var confirmValue = value ?? '';
                          if (confirmValue.isEmpty) {
                            return 'Password cannot be empty';
                          } else if (confirmValue.length < 6) {
                            return 'Password length should be atleast 6';
                          }
                          return null;
                        },
                        onChanged: (value) {},
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      Material(
                        color: primaryColorPurple,
                        borderRadius:
                            BorderRadius.circular(changeButton ? 50 : 8),
                        child: InkWell(
                          onTap: () => moveToHome(context),
                          child: AnimatedContainer(
                            height: 50,
                            width: changeButton ? 50 : 150,
                            alignment: Alignment.center,
                            duration: const Duration(seconds: 1),
                            child: changeButton
                                ? const Icon(
                                    Icons.done,
                                    color: Colors.white,
                                  )
                                : const Text(
                                    'Login',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.white,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
