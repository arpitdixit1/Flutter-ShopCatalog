import 'package:flutter/material.dart';
import 'package:vs_flutter_catalog/utils/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class MyTheme {
  static ThemeData lightTheme(BuildContext context) => ThemeData(
        primarySwatch: primaryColorPurple,
        fontFamily: GoogleFonts.lato().fontFamily,
        appBarTheme: const AppBarTheme(
          color: appBarColor,
          elevation: 0.0,
          iconTheme: IconThemeData(color: black),
          titleTextStyle: TextStyle(
            color: black,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        //primaryTextTheme: GoogleFonts.latoTextTheme(),
      );

  static ThemeData darkTheme(BuildContext context) => ThemeData(
        brightness: Brightness.dark,
      );
}
