// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'package:vs_flutter_catalog/models/catalog.dart';
import 'package:vs_flutter_catalog/utils/constants.dart';

class ItemWidget extends StatelessWidget {
  const ItemWidget({Key? key, required this.item})
      : assert(item != null),
        super(key: key);

  final Item item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0),
      child: Card(
        child: ListTile(
          leading: Image.network(item.image),
          title: Text(item.name),
          subtitle: Text(item.desc),
          trailing: Text(
            "\$${item.price.toString()}",
            textScaleFactor: 1.5,
            style: const TextStyle(
              color: primaryColorPurple,
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {},
        ),
      ),
    );
  }
}
