import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vs_flutter_catalog/utils/constants.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  final imageUrl =
      'https://media-exp1.licdn.com/dms/image/C4E03AQHGY9RVgazw0A/profile-displayphoto-shrink_800_800/0/1631383815505?e=1647475200&v=beta&t=SGpQ70xOQcMSX2ex-AVOyvPqB7rBs-iKvJ_5lzEgp5I';

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: primaryColorPurple,
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: const Text('Arpit Dixit'),
            accountEmail: const Text('arpitdixit1@gmail.com'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(imageUrl),
            ),
          ),
          const ListTile(
            leading: Icon(
              CupertinoIcons.home,
              color: drawerIconsAndTextColor,
            ),
            title: Text(
              'Home',
              textScaleFactor: 1.2,
              style: TextStyle(color: drawerIconsAndTextColor),
            ),
          ),
          const ListTile(
            leading: Icon(
              CupertinoIcons.profile_circled,
              color: drawerIconsAndTextColor,
            ),
            title: Text(
              'Profile',
              textScaleFactor: 1.2,
              style: TextStyle(color: drawerIconsAndTextColor),
            ),
          ),
          const ListTile(
            leading: Icon(
              CupertinoIcons.mail,
              color: drawerIconsAndTextColor,
            ),
            title: Text(
              'Email me',
              textScaleFactor: 1.2,
              style: TextStyle(color: drawerIconsAndTextColor),
            ),
          )
        ],
      ),
    );
  }
}
