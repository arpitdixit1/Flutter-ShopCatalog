import 'package:flutter/material.dart';

// MARK: COLORS --------
// - System
const black = Colors.black;

// - User defines
const appBarColor = Colors.white;
const primaryColorPurple = Colors.deepPurple;
const drawerIconsAndTextColor = Colors.white;
